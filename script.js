/*------basic page setups------*/
let boardmain=document.getElementById("board");
let boardbase=document.getElementById("main");
let boardbox=document.getElementById("board-input");
let boardbutton=document.getElementById("board-button");
let boardclose=document.getElementById("board-close");

boardclose.addEventListener("click",function(){
    boardbox.value="";
    //console.log("closebutton");
})
boardbox.addEventListener("focus",function(){
    boardclose.style.visibility="visible";
})
boardbox.addEventListener("blur",function(){
    boardclose.style.visibility="hidden";
})

//board adding and setting ------->

boardbutton.addEventListener("click",function(){
    
    let dragelement=null;

    const all=document.querySelectorAll(".boardstyle");
    all.forEach(main =>{
      main.addEventListener('dragover',dragover);
      main.addEventListener('drop',dragdrop);
    })
    function dragstart(){
      dragelement=this;
    }
    function dragend(){
      dragelement=null;
    }
    function dragover(e){
      e.preventDefault();
    }
    function dragdrop(){
        this.insertAdjacentElement("beforebegin",dragelement);
    }

    if(boardbox.value=="")
    {
        alert("Field should not be empty");
        //console.log("alert working");
    }
    else{
        const board=document.createElement("div");
        board.classList.add("boardstyle");
        board.draggable="true";
        let a=boardmain.firstChild;
        //boardbase.insertBefore(board,a);
        //boardbase.append(board);
        boardmain.insertAdjacentElement("beforebegin",board);
        //boardbase.append(board);
        //this.parentNode.prepend(board);
        //boardbase.lastChild.prepend(board);
        const boardname=document.createElement("h3");
        boardname.classList.add("board-name")
        boardname.innerHTML=boardbox.value;
        board.appendChild(boardname);
                    const close=document.createElement("span");
                    const closetxt=document.createTextNode("\u00D7");
                    close.classList="close";
                    close.appendChild(closetxt);
         boardname.appendChild(close);           
        boardbox.value="";

        close.addEventListener("click",function(){
            //board.style.display="none";
            board.remove();
        })
        //console.log("board added");
    
        const addcard=document.createElement("span");
        addcard.innerHTML=" + Add Card"
        addcard.classList.add("cardstyle");
        board.appendChild(addcard);

        board.addEventListener("dragstart",dragstart);
        boardname.addEventListener("dragend",dragend);
        /*-----------card adding-----------*/

        addcard.addEventListener("click",function(){
            const cardelement=document.createElement("input");
            cardelement.setAttribute("type","textbox");
            cardelement.classList.add("card-box");
            cardelement.id="textcard";
            board.append(cardelement);
            const cardbutton=document.createElement("input");
            cardbutton.setAttribute("type","button");
            cardbutton.setAttribute("value","Add");
            cardbutton.classList.add("card-button");
            cardbutton.id="addcard";
            board.appendChild(cardbutton);
            const removeadd=document.createElement("span");
            const span_txt1=document.createTextNode("\u00D7");
            removeadd.classList.add("card-close");
            removeadd.appendChild(span_txt1);
            board.append(removeadd);
            cardelement.value="";
            addcard.style.display="none";

            /*-----close button for card adding------*/
            removeadd.addEventListener("click",function(){
                cardelement.value="";
                addcard.style.display="inherit";
                cardelement.style.display="none";
                cardbutton.style.display="none";
                removeadd.style.display="none";
            })
/*------add buttton----*/
cardbutton.addEventListener("click",function(){
                
  let dragelement=null;
  const all=document.querySelectorAll(".liststyle");
  all.forEach(column =>{
    column.addEventListener('dragover',dragover);
    column.addEventListener('drop',dragdrop);
  })
  function dragstart(){
    dragelement=this;
  }
  function dragend(){
    dragelement=null;
  }
  function dragover(e){
    e.preventDefault();
  }
  function dragdrop(){
    this.insertAdjacentElement("beforebegin",dragelement);
    //this.appendChild(dragelement);
  }

                if(cardelement.value==""){
                    alert("Field should not be empty");
                }
                /*------list adding------*/
                else{
                    const listelement=document.createElement("span");
                    listelement.innerHTML=cardelement.value;
                    this.parentNode.append(listelement);

                    const listclose=document.createElement("span");
                    const listclosetxt=document.createTextNode("\u00D7");
                    listclose.classList="close-style";
                    listclose.appendChild(listclosetxt);
                    /*const listedit=document.createElement("h6");
                    listedit.classList.add("edit-icon");
                    listedit.innerHTML='<i class="far fa-edit fa-lg"></i>';*/
                    listelement.appendChild(listclose);
                    //listelement.appendChild(listedit);
                    cardelement.value="";

                    listclose.addEventListener("click",function(){
                        listelement.style.display="none";
                    })

                    listelement.classList.add("liststyle");
                    listelement.draggable="true";
                    listelement.style.userSelect="none";
                    
                    listelement.addEventListener("dragstart",dragstart);
                    listelement.addEventListener("dragend",dragend);
                }
            })
        })
    }
})